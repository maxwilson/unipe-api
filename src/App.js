import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Wrapper from './components/wrapper'
import Beers from './containers/beers'
import BeerDetail from './containers/beerDetail'
import './App.css';


const App = () => (
  <Router>
    <Switch>
      <Wrapper>
        <Route exact path='/' component={Beers} />
        <Route exact path='/beers/:phoneId' component={BeerDetail} />
      </Wrapper>
    </Switch>
  </Router>
)

export default App;
