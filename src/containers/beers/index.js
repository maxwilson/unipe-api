import React, { Component } from 'react'
import BeerItem from '../../components/beerItem'
import apiService from '../../utils/apiService'
import Load from '../../components/load'
import { CardAction } from '../../components/cards'
import Button from '../../components/button'
import './beers.css'

class Beers extends Component {
  state = {
    beerList: [],
    isLoad: true,
  }

  async getBeersList() {
    try { //https://api.punkapi.com/v2/beers?page=1&per_page=30
    	const data = await apiService.get('beers?page=1&per_page=30')
    	this.setState({ beers: data })
    } catch (error) {
    	console.error(error)
    } finally {
      this.setState({
        ...this.state,
        isLoad: false,
      })
    }
  }

  handleClickDetail(id) {
    this.props.history.push(`/beers/${id}/`)
  }

  componentDidMount() {
    this.getBeersList()
  }

  render() {
    return(
      <>
        {this.state.isLoad ? (
          <Load />
        ) : (
          <>
            <div className="container">
              { this.state.beers.map(({ id, name, tagline, image_url}) => (
                <div key={id}>
                  <BeerItem 
                    name={name}
                    tagline={tagline}
                    image_url={image_url}
                  >
                    <CardAction>
                      <Button size="small" onClick={() => this.handleClickDetail(id)}> Detalhes</Button>
                    </CardAction>
                  </BeerItem>
                </div>
              ))}
           </div>
          </>
        )}
      </>
    )
  }
}

export default Beers