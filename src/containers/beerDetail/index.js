import React, { Component } from 'react'
import Header from '../../components/header'
import BeerItem from '../../components/beerItem'
import { CardAction } from '../../components/cards'
import Load from '../../components/load'
import Button from '../../components/button'
import apiService from '../../utils/apiService'

class BeerDetail extends Component {

  state = {
    beerlist: {},
    isLoad: true,
  }

  handleClickBack() {
    this.props.history.push('/')
  }

  async getUserData() {
    try {
      const data = await apiService.get(`beers/${this.props.match.params.phoneId}`)
      this.setState({beerlist: data})
    } catch(error){
      console.error(error)
    } finally {
      this.setState({
        ...this.state,
        isLoad: false
      })
    }
  }

  componentDidMount() {
    this.getUserData()
  }

  render() {
    
    const {beerlist: {name, tagline, image_url,first_brewed,description,ingredients}, isLoad} = this.state
    return (
      <>
        {isLoad ? (
          <Load />
        ):(
          <>
          <Header title={name}>
          </Header>
          <BeerItem 
            tagline={tagline}
            image_url={image_url}
            first_brewed={first_brewed}
            description={description}
            ingredients={ingredients}
          >
            <CardAction>
              <Button onClick={()=> this.handleClickBack()}>Voltar</Button>
            </CardAction>
          </BeerItem>
          </>
        )}
      </>
    );
  }
}

export default BeerDetail