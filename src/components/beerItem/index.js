import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { Card, CardContent } from '../cards'
import '../phoneItem/phoneItem.css'

/* const Phone = ({ type }) => {
  if(type === 'Residencial') return <FontAwesomeIcon icon={faPhone} />
  else return <FontAwesomeIcon icon={faMobileAlt} />
} */
//id, name, tagline, image_url

const BeerItem = ({ name, tagline, image_url,first_brewed,description,ingredients, children }) => {
  return (
    <div className="phoneContainer">
      <Card>
        <CardContent>
          <ul className="phoneItem">
            <li><FontAwesomeIcon icon={faUser} /> {name} </li>
            <li>{tagline}</li>
            <li>
              <img src={image_url} alt={name} height="160" width="60" />
            </li>
            <li>{description}</li>
            <li>{first_brewed}</li>
            <li>{ingredients}</li>
          </ul>
        </CardContent>
        {children}
      </Card>
    </div>
  )
}
export default BeerItem